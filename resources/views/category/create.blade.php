
@extends('layouts.admin')

@section('content')

    <div class="card bg-light rounded-0 my-5">
        <div class="card-header">
            <span class="float-left"><strong>Category</strong>List</span>
            <span class="float-right"><a href="{{ route('categories.index') }}" class="btn btn-primary btn-sm">Create new Category</a></span>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('categories.store') }}">
                <div class="form-group">
                    @csrf
                    <label for="name">Category Name:</label>
                    <input type="text" class="form-control" name="name"/>
                </div>

                <button type="submit" class="btn btn-primary btn-sm">Create</button>
            </form>
        </div>
    </div>



@endsection
