
@extends('layouts.admin')

@section('content')

    <div class="card bg-light rounded-0 my-5">
        <div class="card-header">
            <span class="float-left"><strong>Messages</strong>List</span>
{{--            <span class="float-right"><a href="{{ route('categories.create') }}" class="btn btn-primary btn-sm">Create new Category</a></span>--}}
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">SL</th>
                    <th scope="col">Name</th>
                    <th scope="col">Email</th>
                    <th scope="col">Subject</th>
                    <th scope="col">Text</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($messages as $k=>$message)
                <tr>
                    <th scope="row">{{ $k+1 }}</th>
                    <td>{{ $message->name }}</td>
                    <td>{{ $message->email }}</td>
                    <td>{{ $message->subject }}</td>
                    <td>{{ $message->website }}</td>
                    <td>{{ $message->text }}</td>
                    <td>
                        <a href="{{ route('messages.show', $message->id) }}" class="btn btn-sm btn-primary">Vew</a>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>



@endsection
