<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {

    return view('welcome');
});

/*-----Backend ----------*/

Route::put('categories/{category}/toggle-status', 'CategoryController@toggleStatus')->name('categories.toggleStatus');
Route::resource('categories', 'CategoryController')->except([ 'show']);

Route::resource('posts', 'PostController');

Route::resource('messages', 'MessageController');

Route::resource('subscribes', 'SubscribeController');
