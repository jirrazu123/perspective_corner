
@extends('layouts.admin')

@section('content')

    <div class="card bg-light rounded-0 my-5">
        <div class="card-header">
            <span class="float-left"><strong>Messages</strong>List</span>
            <span class="float-right"><a href="{{ route('messages.index') }}" class="btn btn-primary btn-sm">Go To Messages List</a></span>
        </div>
        <div class="card-body">
                <p><Strong>Name : </Strong>{{ $message->name }}</p>
                <p><Strong>Email : </Strong>{{ $message->email }}</p>
                <p><Strong>Website : </Strong>{{ $message->website ?? "" }}</p>
                <p><Strong>Subject : </Strong>{{ $message->subject ?? "" }}</p>
                <p><Strong>Message : </Strong>{{ $message->message }}</p>
        </div>
    </div>



@endsection
