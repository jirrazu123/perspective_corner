
@extends('layouts.admin')

@section('content')

    <div class="card bg-light rounded-0 my-5">
        <div class="card-header">
            <span class="float-left"><strong>Category</strong>List</span>
            <span class="float-right"><a href="{{ route('categories.create') }}" class="btn btn-primary btn-sm">Create new Category</a></span>
        </div>
        <div class="card-body">
            @if(session()->has('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">SL</th>
                    <th scope="col">Name</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($categories as $k=>$category)
                <tr>
                    <th scope="row">{{ $k+1 }}</th>
                    <td>{{ $category->name }}</td>
                    <td>
                        <form method="post" action="{{ route('categories.toggleStatus', $category->id) }}">
                            @csrf
                            @method('PUT')
                            <button href type="submit" class="btn {{ $category->status ? "btn-success" : "btn-danger" }} btn-sm">{{ $category->status ? "Published" : "Unpublished" }}</button>
                        </form>
                    </td>
                    <td>
                        <form method="post" action="{{ route('categories.destroy', $category->id) }}" >
                            @csrf
                            @method('DELETE')
                            <a class="btn btn-primary btn-sm" href="{{ route('categories.edit', $category->id) }}">Edit</a>
                            <button class="btn btn-danger btn-sm" type="submit">Delete</button>
                        </form>
                    </td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>



@endsection
