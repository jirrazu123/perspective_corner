<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Home</title>

    <!--FAVICON-->
    <link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <link rel="icon" href="images/favicon.png" type="image/x-icon">

    <!-- MOBILE RESPONSIVE META -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="{{ asset('assets/css/bootstrap.min.css') }}">
    <!-- SLICK SLIDER -->
    <link rel="stylesheet" href="{{ asset('assets/css/slick.css') }}">
    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- ANIMATION CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
    <!-- AOS -->
    <link rel="stylesheet" href="{{ asset('assets/css/aos.css') }}">
    <!-- VENOBOX POPUP -->
    <link rel="stylesheet" href="{{ asset('assets/css/venobox.css') }}">
    <!-- CHART JS -->
    <script src="{{ asset('assets/js/Chart.bundle.js') }}"></script>

    <!-- MAIN STYLESHEET -->
    <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">
    @yield('style')
</head>

<body>
<!-- START PRELOADER -->
<div class="preloader">
    <div class="spin"></div>
</div>
<!-- PRELOADER END -->


<!-- START HEADER PART -->
<header>
    <!-- NAVIGATION -->
    <div class="navigation bg-white position-relative  shadow-sm">
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-light bg-white ">
                <a class="navbar-brand" href="index.html"><img class="img-fluid" src="{{ asset('assets/images/logo.png') }}" alt="Perspective corner"></a>
                <button class="navbar-toggler border-0" type="button" data-toggle="collapse" data-target="#navigation"
                        aria-controls="navigation" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse text-center" id="navigation">
                    <ul class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link py-1" href="#contactUS">Logout</a>
                        </li>
                    </ul>
                </div>
            </nav>
        </div>
    </div>
</header>
<!-- HEADER PART END -->
<div class="row">
    <div class="col-md-2">
        <ul class="list-group my-3 ml-3 rounded-0">
            <li class="list-group-item"><a href="{{ route('categories.index') }}">Category</a></li>
            <li class="list-group-item">Post</li>
            <li class="list-group-item"><a href="{{ route('messages.index') }}">Message</a></li>
            <li class="list-group-item"><a href="{{ route('subscribes.index') }}">Subscription</a> </li>
            <li class="list-group-item">Setting</li>
        </ul>
    </div>
    <div class="col-md-10">
        <div class="container">
            @yield('content')
        </div>
    </div>

</div>


<!-- START FOOTER PART -->
<footer>

    <!-- COPYRIGHT -->
    <div class="bg-secondary-darken py-4">
        <div class="container">
            <div class="row">
                <div class="col-md-6 text-center text-md-left mb-3 mb-md-0">
                    <p class="mb-0 text-white"><span class="text-primary">DEBDA</span> &copy; <script>
                            var CurrentYear = new Date().getFullYear()
                            document.write(CurrentYear)
                        </script> All Right Reserved</p>
                </div>
                <div class="col-md-6 text-center text-md-right">

                </div>
            </div>
        </div>
    </div>
</footer>
<!-- FOOTER PART END  -->


<!-- START BODY SCROLL TO UP SECTION -->
<div class="body-scrollup" data-wow-duration="2s">
    <span class="fa fa-chevron-up"></span>
</div>
<!-- BODY SCROLL TO UP SECTION END -->




<!-- JQUERY -->
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<!-- BOOTSTRAP JS -->
<script src="{{ asset('assets/js/bootstrap.min.js') }}"></script>
<!-- SLICK SLIDER -->
<script src="{{ asset('assets/js/slick.min.js') }}"></script>
<!-- AOS -->
<script src="{{ asset('assets/js/aos.js') }}"></script>
<!-- VENOBOX POPUP -->
<script src="{{ asset('assets/js/venobox.min.js') }}"></script>
<!-- MODERNIZER -->
<script src="{{ asset('assets/js/modernizr.min.js') }}"></script>
<!-- FILTER -->
<script src="{{ asset('assets/js/jquery.filterizr.min.js') }}"></script>
<!-- GOOGLE MAP -->
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBu5nZKbeK-WHQ70oqOWo-_4VmwOwKP9YQ"></script>
<script src="{{ asset('assets/js/gmap.js') }}"></script>


<!-- MAIN SCRIPT -->
<script src="{{ asset('assets/js/script.js') }}"></script>

</body>
</html>
