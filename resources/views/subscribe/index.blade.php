
@extends('layouts.admin')

@section('content')

    <div class="card bg-light rounded-0 my-5">
        <div class="card-header">
            <span class="float-left"><strong>Subscribes</strong>Message List</span>
        </div>
        <div class="card-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col">SL</th>
                    <th scope="col">Email List</th>
                </tr>
                </thead>
                <tbody>
                @foreach($subscribes as $k=>$subscribe)
                <tr>
                    <th scope="row">{{ $k+1 }}</th>
                    <td>{{ $subscribe->email }}</td>
                </tr>
                @endforeach

                </tbody>
            </table>
        </div>
    </div>



@endsection
